#!/bin/python

import sys


n = int(raw_input().strip())
B = raw_input().strip()

# print '%s and %s' % (n, B)


i = 0
result_count = 0
result_list = list(B)
# print "result_list=%s" % result_list
result_string = "".join(result_list)
while i + 3 <= n:
	substring =  result_string[i:i+3]
	# print "substring= %s " % substring
	if substring == "010":
		# print result
		# print "on index: %s" % i 
		result_list[i+2] = "1"
		result_string = "".join(result_list)
		result_count+=1
	i+=1

# print result_string
# print "result count: %s" % result_count
print result_count