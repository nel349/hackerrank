#https://www.hackerrank.com/challenges/camelcase
string = raw_input()

index = 0;
wordList = []
beginningOfPreviousWord = 0;
for l in string:
	if (l.isupper()):
		wordList.append(string[beginningOfPreviousWord:index])
		beginningOfPreviousWord = index
	if (index == len(string)-1 ):
		wordList.append(string[beginningOfPreviousWord: index+1])
	index+=1

print len(wordList)