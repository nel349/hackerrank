# https://www.hackerrank.com/challenges/reduced-string
def solve(a, checkAgain):
	if(len(a) == 0):
		print "Empty String"
		return
	if not checkAgain:
		print a
		return

	checkAgain = False
	i=0
	temp = list(a)
	while i+1 < len(a):
		# print "%s %s and %s %s" % (i,a[i],i+1,a[i+1])
		if a[i] == a[i+1]:
			checkAgain = True
			# print "%s %s a nd %s %s" % (i,a[i],i+1,a[i+1])
			# print "%s %s" % (a[:i], a[i+2:])
			temp[i] = ""
			temp[i+1] = ""
			i+=2
		else:
			i+=1

		# print temp

		
	solve("".join(temp), checkAgain)

	

a = raw_input()

solve(a, True)
