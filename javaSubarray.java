//https://www.hackerrank.com/challenges/java-negative-subarray
import java.io.*;
import java.util.*;

public class javaSubarray {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    	Vector<Vector<Integer>> subarrays = new Vector<Vector<Integer>>();

    	Vector<Integer> array = new Vector<Integer>();



    	Scanner sc = new Scanner(System.in);

    	int N = sc.nextInt(); //number of element in the array

    	for (int i = 0; i < N; i++) {
    		int el = sc.nextInt();
    		array.add(el);
    	}


    	for (int start = 0; start < N; start++) {
    		for (int end =0; end < N; end++) {
    			Vector<Integer> sub = new Vector<Integer>();
    			for (int i = start; i <= end; i++) {
    				sub.add(array.get(i));
    			}
    			if (!sub.isEmpty() && (sumVector(sub) < 0)) {
					subarrays.add(sub);
    			}
    		}
    	}

    	// System.out.println(subarrays);
    	System.out.println(subarrays.size());


    	// System.out.println("This is my vector array " + array);
    }


    static int sumVector(Vector<Integer> v) {
    	int sum = 0;
    	for (int i =0; i < v.size(); i++) {
    		sum+=v.get(i);
    	}

    	return sum;
    }
}