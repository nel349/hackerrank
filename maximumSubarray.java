https://www.hackerrank.com/challenges/maxsubarray
import java.io.*;
import java.util.*;

public class maximumSubarray {

    static Vector<Vector<Integer>> subarraysContinuous = new Vector<Vector<Integer>>();
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        

        Vector<Integer> array = new Vector<Integer>();



        Scanner sc = new Scanner(System.in);

        int T = sc.nextInt(); //number of Testcases

        // System.out.println("Testcases: " + T);
        for (int t = 0 ; t < T; t++) {

            int N = sc.nextInt(); //number of element in the array
            // System.out.println(N);
            for (int i = 0; i < N; i++) {
                int el = sc.nextInt();
                array.add(el);
                // System.out.print(" " + el);
            }
            solve(array, N);
            // System.out.println(array);

            System.out.println();
            subarraysContinuous.clear(); //reset per Testcase
            array.clear(); //Reset per testcase
        }
    }

    static void solve(Vector<Integer> array, int N) {
        int globalMaxSum = -999999999;
        int currentMaxSum = 0;
        int nonContinuous = 0;
        for (int i =0; i < N; i++) {
            currentMaxSum+= array.get(i);

            if (array.get(i) > 0) {
                nonContinuous +=  array.get(i);
            }

            if ( currentMaxSum > globalMaxSum ) {
                globalMaxSum = currentMaxSum;
            }


            if (currentMaxSum < 0) {
                currentMaxSum = 0;
            }


        }

        if (nonContinuous == 0) {
            nonContinuous = (int)Collections.max(array);
        }
        System.out.print(globalMaxSum + " " + nonContinuous);
    }

}